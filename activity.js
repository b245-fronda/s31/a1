// 1. What directive is used by Node.js in loading the modules it needs?

// answer: require

// 2. What Node.js module contains a method for server creation?

// answer: http

// 3. What is the method of the http object responsible for creating a server using Node.js?

// answer: createServer

// 4. What method of the response object allows us to set status codes and content types?
	
// answer: writeHead

// 5. Where will console.log() output its contents when run in Node.js?

// answer: web browser console

// 6. What property of the request object contains the address's endpoint?

// answer: url

const http = require('http');
const port = 3000;
const server = http.createServer((req, res) => {

	if(req.url == '/login') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Welcome to the login page.");
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end("I'm sorry the page you are looking for cannot be found.");
	}
});

server.listen(port);

console.log(`Server now acccessible at localhost: ${port}`);